function analyze()
{
	$packages = GetPackages
	$groupedPackgeList = $packages | Group-Object -Property Id
	$multipleVersionPackageList = @()
	$groupedPackgeList | % {
	    $originalCount =$_.Count
	    $groupByVersion = $_.Group | Group-Object -Property Version
	    if(@($groupByVersion).Count -gt 1)
	    {
		    $packageName = $_.Name
		    $groupByVersion |% {
			        $_.Group |% {
			            $tempObj = New-Object PSCustomObject -Property @{Id=$packageName;Version=$_.Version;Config=$_.Config;}    
			            $multipleVersionPackageList+=$tempObj  
			        }
		    } 
	    }
    }
    $multipleVersionPackageList
}


function GetPackages()
{
	$rootDir = Convert-Path .
	$rootPath = "$rootDir"
	$Dir = get-childitem $rootPath -recurse
	$packagesList = $Dir | where {$_.BaseName -eq "packages" -and $_.Extension -eq ".config"}
	$allPackageList=@()

	$packagesFullNameList = $packagesList | select -Property FullName,Directory
	$packagesFullNameList | %{
				if($_.FullName.Contains("\obj\") -ne $true -and $_.FullName.Contains(".nuget") -ne $true)
				{
					[xml]$packageDeserialized = Get-Content $_.FullName
					$currentPackageName = Get-ChildItem $_.Directory | where {$_.Extension -eq ".csproj"}
					$packageName =$currentPackageName
					$packagesDetailList = $packageDeserialized.packages | % {$_.package}
					$packagesDetailList |% {
					$currentPacakgeObject = New-Object PSCustomObject  -Property @{Config=$packageName;Id=$_.id;Version=$_.Version}
					$allPackageList+=$currentPacakgeObject
				}
		}
	}
	return $allPackageList
}

function Version($packageName)
{
    $filteredPackages =  GetPackages | where {$_.Id -eq "$packageName"}
    return $filteredPackages
}